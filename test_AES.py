from unittest import TestCase

from aes import AES, key_expansion


class TestAES(TestCase):
    def test_padding(self):
        padded = AES._pad_string("1234567891")  # len: 10
        self.assertTrue(padded.endswith(b'\x80\x00\x00\x00\x00\x00'), "zero padding")

    def test_max_padding(self):
        padded = AES._pad_string("1234567891234567")  # len: 16, so the next 16 bytes are padding
        self.assertTrue(padded.endswith(b'\x80\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'),
                        "zero padding")

    def test_text2matrix(self):
        matrix = AES.bytes2matrix(AES._pad_string("12345678"))
        self.assertTrue(matrix == [[49, 50, 51, 52], [53, 54, 55, 56], [128, 0, 0, 0], [0, 0, 0, 0]])

    def test_improper_key_len(self):
        sut = AES()
        sut.encrypt("1111111111111111", "213123")
        self.assertRaises(KeyError)

    def test_128bit_key_len(self):
        sut = AES()
        sut.encrypt("000102030405060708090a0b0c0d0e0f", "00112233445566778899aabbccddeeff", True)
        self.assertTrue(sut is not None)

    def test128bit_key_expansion_consecutive_bytes(self):
        sut = key_expansion(
            bytearray(b"\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f"))
        print("sut: ")
        sut_str = ' '.join('{:02x}'.format(x) for x in sut)
        print(sut_str)
        proper = """00 01 02 03 04 05 06 07 08 09 0a 0b 0c 0d 0e 0f d6 aa 74 fd d2 af 72 fa da a6 78 f1 d6 ab 76 fe b6 92 cf 0b 64 3d bd f1 be 9b c5 00 68 30 b3 fe b6 ff 74 4e d2 c2 c9 bf 6c 59 0c bf 04 69 bf 41 47 f7 f7 bc 95 35 3e 03 f9 6c 32 bc fd 05 8d fd 3c aa a3 e8 a9 9f 9d eb 50 f3 af 57 ad f6 22 aa 5e 39 0f 7d f7 a6 92 96 a7 55 3d c1 0a a3 1f 6b 14 f9 70 1a e3 5f e2 8c 44 0a df 4d 4e a9 c0 26 47 43 87 35 a4 1c 65 b9 e0 16 ba f4 ae bf 7a d2 54 99 32 d1 f0 85 57 68 10 93 ed 9c be 2c 97 4e 13 11 1d 7f e3 94 4a 17 f3 07 a7 8b 4d 2b 30 c5"""
        print("Proper:")
        print(proper)
        self.assertTrue(sut_str == proper)

    def test192bit_key_expansion_consecutive_bytes(self):
        sut = key_expansion(
            bytearray(
                b"\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f\x10\x11\x12\x13\x14\x15\x16\x17"))
        print("sut: ")
        sut_str = ' '.join('{:02x}'.format(x) for x in sut)
        print(sut_str)
        proper = """00 01 02 03 04 05 06 07 08 09 0a 0b 0c 0d 0e 0f 10 11 12 13 14 15 16 17 58 46 f2 f9 5c 43 f4 fe 54 4a fe f5 58 47 f0 fa 48 56 e2 e9 5c 43 f4 fe 40 f9 49 b3 1c ba bd 4d 48 f0 43 b8 10 b7 b3 42 58 e1 51 ab 04 a2 a5 55 7e ff b5 41 62 45 08 0c 2a b5 4b b4 3a 02 f8 f6 62 e3 a9 5d 66 41 0c 08 f5 01 85 72 97 44 8d 7e bd f1 c6 ca 87 f3 3e 3c e5 10 97 61 83 51 9b 69 34 15 7c 9e a3 51 f1 e0 1e a0 37 2a 99 53 09 16 7c 43 9e 77 ff 12 05 1e dd 7e 0e 88 7e 2f ff 68 60 8f c8 42 f9 dc c1 54 85 9f 5f 23 7a 8d 5a 3d c0 c0 29 52 be ef d6 3a de 60 1e 78 27 bc df 2c a2 23 80 0f d8 ae da 32 a4 97 0a 33 1a 78 dc 09 c4 18 c2 71 e3 a4 1d 5d"""
        print("proper: ")
        print(proper)
        self.assertTrue(sut_str == proper)

    def test256bit_key_expansion(self):
        sut = key_expansion(
            bytearray(
                b"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"))
        sut_str = ' '.join('{:02x}'.format(x) for x in sut)
        proper = """00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 62 63 63 63 62 63 63 63 62 63 63 63 62 63 63 63 aa fb fb fb aa fb fb fb aa fb fb fb aa fb fb fb 6f 6c 6c cf 0d 0f 0f ac 6f 6c 6c cf 0d 0f 0f ac 7d 8d 8d 6a d7 76 76 91 7d 8d 8d 6a d7 76 76 91 53 54 ed c1 5e 5b e2 6d 31 37 8e a2 3c 38 81 0e 96 8a 81 c1 41 fc f7 50 3c 71 7a 3a eb 07 0c ab 9e aa 8f 28 c0 f1 6d 45 f1 c6 e3 e7 cd fe 62 e9 2b 31 2b df 6a cd dc 8f 56 bc a6 b5 bd bb aa 1e 64 06 fd 52 a4 f7 90 17 55 31 73 f0 98 cf 11 19 6d bb a9 0b 07 76 75 84 51 ca d3 31 ec 71 79 2f e7 b0 e8 9c 43 47 78 8b 16 76 0b 7b 8e b9 1a 62 74 ed 0b a1 73 9b 7e 25 22 51 ad 14 ce 20 d4 3b 10 f8 0a 17 53 bf 72 9c 45 c9 79 e7 cb 70 63 85"""
        self.assertTrue(sut_str == proper)

    def test_encrypt_128bitkey_spec(self):
        # https://csrc.nist.gov/csrc/media/publications/fips/197/final/documents/fips-197.pdf C1 p. 39
        key = "000102030405060708090a0b0c0d0e0f"
        message = "00112233445566778899aabbccddeeff"
        sut = AES()
        cipher = sut.encrypt(key, message, AES.Mode.ECB, True)
        sut_str = ' '.join('{:02x}'.format(x) for x in cipher)
        proper_cipher = "69 c4 e0 d8 6a 7b 04 30 d8 cd b7 80 70 b4 c5 5a"
        print("cipher:")
        print(sut_str)
        print("proper")
        print(proper_cipher)
        self.assertTrue(proper_cipher == sut_str, "encrypt 128bit key")

    def test_encrypt_192bitkey_spec(self):
        # https://csrc.nist.gov/csrc/media/publications/fips/197/final/documents/fips-197.pdf C1 p. 39
        key = "000102030405060708090a0b0c0d0e0f1011121314151617"
        message = "00112233445566778899aabbccddeeff"
        sut = AES()
        cipher = sut.encrypt(key, message, AES.Mode.ECB, True)
        sut_str = ' '.join('{:02x}'.format(x) for x in cipher)
        proper_cipher = "dd a9 7c a4 86 4c df e0 6e af 70 a0 ec 0d 71 91"
        print("cipher:")
        print(sut_str)
        print("proper")
        print(proper_cipher)
        self.assertTrue(proper_cipher == sut_str, "encrypt 192bit key")

    def test_decrypt_192bitkey_spec(self):
        key = "000102030405060708090a0b0c0d0e0f1011121314151617"
        cryptogram = "dda97ca4864cdfe06eaf70a0ec0d7191"
        message = "00 11 22 33 44 55 66 77 88 99 aa bb cc dd ee ff"
        sut = AES()
        sut_message = sut.decrypt(key, cryptogram)
        sut_str = ' '.join('{:02x}'.format(x) for x in sut_message)
        print("message:")
        print(sut_str)
        print("proper message")
        print(message)
        self.assertTrue(message == sut_str, "dencrypt using 192bit key")

    def test_encrypt_256bitkey_spec(self):
        # https://csrc.nist.gov/csrc/media/publications/fips/197/final/documents/fips-197.pdf C1 p. 39
        key = "000102030405060708090a0b0c0d0e0f101112131415161718191a1b1c1d1e1f"
        message = "00112233445566778899aabbccddeeff"
        sut = AES()
        cipher = sut.encrypt(key, message, AES.Mode.ECB, True)
        sut_str = ' '.join('{:02x}'.format(x) for x in cipher)
        proper_cipher = "8e a2 b7 ca 51 67 45 bf ea fc 49 90 4b 49 60 89"
        print("cipher:")
        print(sut_str)
        print("proper")
        print(proper_cipher)
        self.assertTrue(proper_cipher == sut_str, "encrypt 256bit key")

    def test_decrypt_256bitkey_spec(self):
        key = "000102030405060708090a0b0c0d0e0f101112131415161718191a1b1c1d1e1f"
        cryptogram = "8ea2b7ca516745bfeafc49904b496089"
        message = "00 11 22 33 44 55 66 77 88 99 aa bb cc dd ee ff"
        sut = AES()
        sut_message = sut.decrypt(key, cryptogram)
        sut_str = ' '.join('{:02x}'.format(x) for x in sut_message)
        print("message:")
        print(sut_str)
        print("proper message")
        print(message)
        self.assertTrue(message == sut_str, "decrypt using 256bit key")

    def test_decryption_128bitkey_spec(self):
        key = "000102030405060708090a0b0c0d0e0f"
        cryptogram = "69c4e0d86a7b0430d8cdb78070b4c55a"
        message = "00 11 22 33 44 55 66 77 88 99 aa bb cc dd ee ff"
        sut = AES()
        sut_message = sut.decrypt(key, cryptogram)
        sut_str = ' '.join('{:02x}'.format(x) for x in sut_message)
        print("message:")
        print(sut_str)
        print("proper message")
        print(message)
        self.assertTrue(message == sut_str, "decrypt using 128bit key")

    def test_ECB_encrypt_two_blocks(self):
        key = "000102030405060708090a0b0c0d0e0f"
        message = "00112233445566778899aabbccddeeff00112233445566778899aabbccddeeff"
        sut = AES()
        cipher = sut.encrypt(key, message, AES.Mode.ECB, True)
        sut_str = ' '.join('{:02x}'.format(x) for x in cipher)
        proper_cipher = "69 c4 e0 d8 6a 7b 04 30 d8 cd b7 80 70 b4 c5 5a 69 c4 e0 d8 6a 7b 04 30 d8 cd b7 80 70 b4 c5 5a"
        print("cipher:")
        print(sut_str)
        print("proper")
        print(proper_cipher)
        self.assertTrue(proper_cipher == sut_str, "encrypt 128bit key CBC two blocks")

    def test_ECB(self):
        key = "000102030405060708090a0b0c0d0e0f"
        message = "8ea2b7ca516745bfeafc49904b496089"
        iv = "0011223344556677889900112233445566"
        sut = AES()
        encrypted = sut.encrypt(key, message, AES.Mode.ECB, True, iv)
        encrypted_str = ''.join('{:02x}'.format(x) for x in encrypted)

        decrypted = sut.decrypt(key, encrypted_str, AES.Mode.ECB, iv)
        decrypted_str = sut.bytes2str(decrypted)
        self.assertTrue(message == decrypted_str, "ECB test")

    def test_PCBC(self):
        key = "000102030405060708090a0b0c0d0e0f"
        message = "8ea2b7ca516745bfeafc49904b496089"
        iv = "11223344556677889911223344556677"

        sut = AES()
        encrypted = sut.encrypt(key, message, AES.Mode.PCBC, True, iv)
        encrypted_str = sut.bytes2str(encrypted)

        decrypted = sut.decrypt(key, encrypted_str, AES.Mode.PCBC, iv)
        decrypted_str = sut.bytes2str(decrypted)
        self.assertTrue(message == decrypted_str, "PCBC test")

    def test_CBC(self):
        iv = "11223344556677889911223344556677"
        key = "000102030405060708090a0b0c0d0e0f"
        message = "8ea2b7ca516745bfeafc49904b496089"

        sut = AES()
        encrypted = sut.encrypt(key, message, AES.Mode.CBC, True, iv)
        encrypted_str = sut.bytes2str(encrypted)

        decrypted = sut.decrypt(key, encrypted_str, AES.Mode.CBC, iv)
        decrypted_str = sut.bytes2str(decrypted)
        self.assertTrue(message == decrypted_str, "CBC test")

    def test_CTR(self):
        key = "000102030405060708090a0b0c0d0e0f"
        message = "8ea2b7ca516745bfeafc49904b496089"
        iv = "11223344556677889911223344556677"

        sut = AES()
        encrypted = sut.encrypt(key, message, AES.Mode.CTR, True, iv)
        encrypted_str = sut.bytes2str(encrypted)

        decrypted = sut.decrypt(key, encrypted_str, AES.Mode.CTR, iv)
        decrypted_str = sut.bytes2str(decrypted)
        self.assertTrue(message == decrypted_str, "CTR test")
